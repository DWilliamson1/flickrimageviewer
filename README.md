# Flickr Image Viewer

This is native iOS application written in Swift 5. It uses the FlickrAPI to present relevant images based on a user location.

Third Party Dependencies Used:

- RxSwift
- RxCocoa
- RxOptional
- RxTest
- SnapKit

## Getting Started

### Prerequisites

-  [Install Cocoapods](https://guides.cocoapods.org/using/getting-started.html)

### Installing

1. Download the dependencies:

```
pod install
```

2. Launch the main application target `FlickrImageViewer`

## Running the tests

There are `11` unit tests in total. They can be executed by using `CMD + U` while in Xcode.

## Future Development

- Pagination
- Tag filtering
- QLPreviewController
- UI Tweaks to make use of the whole screen on larger devices



