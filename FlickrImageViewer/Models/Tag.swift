//
//  Tag.swift
//  FlickrImageViewer
//
//  Created by Daniel Williamson on 22/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import Foundation

public struct Tag: Decodable, Hashable {
	public let id: String
	public let raw: String
}
