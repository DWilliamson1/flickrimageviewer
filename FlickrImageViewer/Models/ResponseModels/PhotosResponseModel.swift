//
//  PhotosResponseModel.swift
//  FlickrImageViewer
//
//  Created by Daniel Williamson on 20/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import Foundation

public struct PhotosResponseModel: Decodable {
	var photos: [Photo] = []
	
	public init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
		if let photosValues = try? values.nestedContainer(keyedBy: PhotosCodingKeys.self, forKey: .photos) {
			
			photos = try photosValues.decode([Photo].self, forKey: .photos)
        }
    }
    
    enum CodingKeys: String, CodingKey {
        case photos
    }
	
	enum PhotosCodingKeys: String, CodingKey {
		case photos = "photo"
	}
}
