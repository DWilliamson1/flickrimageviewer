//
//  TagsResponseModel.swift
//  FlickrImageViewer
//
//  Created by Daniel Williamson on 22/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import Foundation

public struct TagsResponseModel: Decodable {
	
	var tags: [Tag] = []
	
	public init(from decoder: Decoder) throws {
		
		let values = try decoder.container(keyedBy: CodingKeys.self)
		
		if let tagsValues = try? values.nestedContainer(keyedBy: TagsCodingKeys.self, forKey: .photo),
			let tagValues = try? tagsValues.nestedContainer(keyedBy: TagCodingKey.self, forKey: .tags) {
			
			tags = try tagValues.decode([Tag].self, forKey: .tags)
		}
	}
	
	enum CodingKeys: String, CodingKey {
		case photo
	}
	
	enum TagsCodingKeys: String, CodingKey {
		case tags
	}
	
	enum TagCodingKey: String, CodingKey {
		case tags = "tag"
	}
}
