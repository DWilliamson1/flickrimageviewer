//
//  PhotoSizesResponseModel.swift
//  FlickrImageViewer
//
//  Created by Daniel Williamson on 22/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import Foundation

public struct PhotoSizesResponseModel: Decodable {
	
	var sizes: [PhotoSize] = []
	
	public init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
		if let sizesValues = try? values.nestedContainer(keyedBy: SizesCodingKeys.self, forKey: .sizes) {
			
			sizes = try sizesValues.decode([PhotoSize].self, forKey: .sizes)
        }
    }
    
    enum CodingKeys: String, CodingKey {
        case sizes
    }
	
	enum SizesCodingKeys: String, CodingKey {
		case sizes = "size"
	}
}
