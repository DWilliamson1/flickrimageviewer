//
//  File.swift
//  FlickrImageViewer
//
//  Created by Daniel Williamson on 21/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//


public struct PhotosRequestModel {
	
	public let location: Location
	public let query: String?
	
	public let limit: Int
	public let offset: Int
}
