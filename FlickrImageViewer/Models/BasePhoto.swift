//
//  BasePhoto.swift
//  FlickrImageViewer
//
//  Created by Daniel Williamson on 21/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

protocol BasePhoto: Decodable {
	var id: String { get }
	var secret: String { get }
	var server: String { get }
	var farm: Int { get }
}
