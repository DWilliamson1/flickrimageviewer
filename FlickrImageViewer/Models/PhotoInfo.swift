//
//  PhotoInfo.swift
//  FlickrImageViewer
//
//  Created by Daniel Williamson on 21/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

public struct PhotoInfo: BasePhoto {
	public let id: String
	public let secret: String
	public let server: String
	public let farm: Int
	
	let dateUploaded: String
	
	let owner: PhotoOwner
	let dates: PhotoDates

	let location: Location
	
	enum CodingKeys: String, CodingKey {
		case id
		case secret
		case server
		case farm
        case dateUploaded = "dateuploaded"
		case owner
		case dates
		case location
    }
	
	public init(id: String = "", secret: String = "", server: String = "", farm: Int = 0, owner: PhotoOwner) {
		self.id = id
		self.secret = secret
		self.server = server
		self.farm = farm
		self.owner = owner
		
		dateUploaded = ""
		dates = PhotoDates(posted: "", taken: "")
		location = Location.init(latitude: "", longitude: "")
	}
}

public struct PhotoOwner: Decodable {
	let nsid: String
	let username: String
	let realName: String
	let iconServer: String
	
	enum CodingKeys: String, CodingKey {
		case nsid
		case username
        case realName = "realname"
        case iconServer = "iconserver"
    }
	
	public init(nsid: String, username: String, realName: String, iconServer: String) {
		self.nsid = nsid
		self.username = username
		self.realName = realName
		self.iconServer = iconServer
	}
}

public struct PhotoDates: Decodable {
	let posted: String
	let taken: String
	
	public init(posted: String, taken: String) {
		self.posted = posted
		self.taken = taken
	}
}
