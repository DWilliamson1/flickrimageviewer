//
//  PhotoSize.swift
//  FlickrImageViewer
//
//  Created by Daniel Williamson on 22/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import Foundation

public struct PhotoSize: Decodable {
	let label: String
	let width: Int
	let height: Int
	let url: String
}
