//
//  Location.swift
//  Flickr Image Viewer
//
//  Created by Daniel Williamson on 19/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import Foundation
import CoreLocation

public struct Location: Decodable {
	public let latitude: String
	public let longitude: String
	
	public init(fromCLLocation location: CLLocation) {
		self.latitude = String(location.coordinate.latitude)
		self.longitude = String(location.coordinate.longitude)
	}
	
	public init(latitude: String, longitude: String) {
		self.latitude = latitude
		self.longitude = longitude
	}
	
	public func toCLLocation() -> CLLocation? {
		guard let latitude = Double(latitude), let longitude = Double(longitude) else {
			return nil
		}
		
		return CLLocation(latitude: latitude, longitude: longitude)
	}
}
