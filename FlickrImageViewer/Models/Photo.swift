//
//  Photo.swift
//  FlickrImageViewer
//
//  Created by Daniel Williamson on 20/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import Foundation

public enum ImageSize: String {
	case small = "n"
	case large = "b"
	case original = "o"
	case thumbnail = "t"
}

public struct Photo: BasePhoto {
	let id: String
	let secret: String
	let server: String
	let farm: Int
	let title: String
	
	public init(id: String = "", secret: String = "", server: String = "", farm: Int = 0, title: String = "") {
		self.id = id
		self.secret = secret
		self.server = server
		self.farm = farm
		self.title = title
	}
}
