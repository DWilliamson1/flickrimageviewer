//
//  HomeScreenViewModel.swift
//  FlickrImageViewer
//
//  Created by Daniel Williamson on 20/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import Foundation
import RxSwift
import RxOptional

public protocol HomeScreenViewModelType {
	var photos: Observable<[Photo]> { get }
	var isFetching: Observable<Bool> { get }
}

public struct HomeScreenViewModelInputs {
	let searchQuery: Observable<String?>
	let loadNextPage: Observable<Void>
}

public final class HomeScreenViewModel: HomeScreenViewModelType {
	
	private enum Constants {
		static let fetchLimit = 200
	}
	
	public let photos: Observable<[Photo]>

	public var isFetching: Observable<Bool> {
		return isFetchingSubject.asObservable()
	}
	
	private let disposeBag = DisposeBag()
	
	private let isFetchingSubject = BehaviorSubject<Bool>(value: false)
	private let offset = BehaviorSubject<Int>(value: 0)
	private let limit = BehaviorSubject<Int>(value: Constants.fetchLimit)
	
	private let locationManager: LocationManagerType
	private let flickrAPI: APISession<FlickrAPI>
	
	public init(flickrAPI: APISession<FlickrAPI>, locationManager: LocationManagerType, inputs: HomeScreenViewModelInputs) {
		self.flickrAPI = flickrAPI
		self.locationManager = locationManager
		
		let initialLocation = locationManager.currentLocation
			.filterNil()
			.take(1)
		
		photos = Observable.combineLatest(initialLocation, inputs.searchQuery, limit, offset, resultSelector: { ($0, $1, $2, $3) })
			.do(onNext: { [weak isFetchingSubject] _ in
				isFetchingSubject?.onNext(true)
			})
			.requestImagesFromLocation(flickrAPI: self.flickrAPI)
			.map { $0.photos }
			.do(onNext: { [weak isFetchingSubject] _ in
				isFetchingSubject?.onNext(false)
			})
			.share()
		
		setup()
	}
	
	init() {
		flickrAPI = APISession<FlickrAPI>()
		locationManager = LocationManager()
		photos = Observable.empty()
	}
	
	private func setup() {
		locationManager.requestLocation()
	}
}

// Custom Rx operator to fetch images based on location, limit and offet
extension ObservableType where Element == (location: Location, query: String?, limit: Int, offset: Int) {
	
	func requestImagesFromLocation(flickrAPI: APISession<FlickrAPI>) -> Observable<PhotosResponseModel> {
		
		return self.flatMap { combined -> Single<PhotosResponseModel> in
			
			let request = PhotosRequestModel(location: combined.location,
											 query: combined.query,
											 limit: combined.limit,
											 offset: combined.offset)
			
			return flickrAPI.request(.getImages(request), resultObjectType: PhotosResponseModel.self)
		}
	}
}
