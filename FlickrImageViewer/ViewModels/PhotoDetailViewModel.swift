//
//  PhotoDetailViewModel.swift
//  FlickrImageViewer
//
//  Created by Daniel Williamson on 22/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import Foundation
import RxSwift
import RxOptional

public protocol PhotoDetailViewModelType {
	var imageURL: Observable<URL?> { get }
	var imageName: Observable<String> { get }
	var imageSize: Observable<PhotoSize?> { get }
	var imageLocation: Observable<Location> { get }
	
	var avatarURL: Observable<URL?> { get }
	var username: Observable<String> { get }
	var realName: Observable<String> { get }
}

public final class PhotoDetailViewModel: PhotoDetailViewModelType {
	
	public var imageURL: Observable<URL?> {
		return Observable.just(photo.buildImageURLSource(imageSize: .large))
	}
	
	public var imageName: Observable<String>  {
		return Observable.just(photo.title)
	}
		
	public var imageUploadDate: Observable<String?> {
		return photoInfo
			.map { $0.dateUploaded }
			.formatToDateString()
	}
	
	public var imageLocation: Observable<Location> {
		return photoInfo
			.map { $0.location }
	}
	
	public var avatarURL: Observable<URL?> {
		return photoInfo
			.map { $0.buildAvatarURLSource() }
	}
	
	public var username: Observable<String> {
		return photoInfo
			.map { "@\($0.owner.username)" }
	}
	
	public var realName: Observable<String> {
		return photoInfo
			.map { $0.owner.realName }
	}
	
	public let imageSize: Observable<PhotoSize?>
	
	private let photoInfo: Observable<PhotoInfo>
	private let photo: Photo
	private let flickrAPI: APISession<FlickrAPI>
	
	public init(flickrAPI: APISession<FlickrAPI>, photo: Photo) {
		self.flickrAPI = flickrAPI
		self.photo = photo
		
		photoInfo = flickrAPI.request(.getPhotoInformation(photo), resultObjectType: PhotoInfoResponseModel.self)
			.map { $0.photo }
			.asObservable()
			.share()

		
		imageSize = flickrAPI.request(.getPhotoSizes(photo), resultObjectType: PhotoSizesResponseModel.self)
			.map { $0.sizes.first(where: { $0.label == "Original" }) }
			.asObservable()
			.share()
	}
	
	public init() {
		self.flickrAPI = APISession<FlickrAPI>()
		self.photo = Photo()
		
		photoInfo = Observable.never()
		imageSize = Observable.never()
	}
}

// Custom Rx operator to fetch images based on location, limit and offet
extension ObservableType where Element == String {
	
	func formatToDateString() -> Observable<String?> {
		
		return self.map { sourceDate -> String? in
			guard let time = TimeInterval(sourceDate) else {
				return nil
			}
			
			let dateFormatter = DateFormatter()
			dateFormatter.dateStyle = .medium
			dateFormatter.timeStyle = .none
			
			let date = Date(timeIntervalSince1970: time)
			
			return dateFormatter.string(from: date)
		}
	}
}
