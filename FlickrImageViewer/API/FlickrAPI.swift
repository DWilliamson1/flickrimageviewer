//
//  FlickrAPI.swift
//  Flickr Image Viewer
//
//  Created by Daniel Williamson on 18/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import Foundation

public enum FlickrAPI {
	case getImages(_ request: PhotosRequestModel)
	case getPhotoInformation(_ photo: Photo)
	case getPhotoSizes(_ photo: Photo)
}

extension FlickrAPI: APIClient {
	
	private enum Constants {
		static let baseURL = "https://api.flickr.com/services"
		
		//Methods
		static let photosSearch = "flickr.photos.search"
		static let getInfo = "flickr.photos.getInfo"
		static let getSizes = "flickr.photos.getSizes"
	}
	
	public var baseURL: URL {
		return URL(string: Constants.baseURL)!
	}
	
	public var path: String {
		return "/rest/"
	}
	
	public var baseParameters: [String: Any] {
		
		var parameters: [String: Any] = [
			"api_key": Obfuscator.reveal(key: GlobalConstants.flickrAPIKey),
			"format": "json",
			"nojsoncallback": true
		]
		
		switch self {
		case let .getImages(request):
			parameters["method"] = Constants.photosSearch
			parameters["per_page"] = request.limit
			parameters["page"] = request.offset
			
		case .getPhotoInformation:
			parameters["method"] = Constants.getInfo
			
		case .getPhotoSizes:
			parameters["method"] = Constants.getSizes
		}
		
		return parameters
	}
	
	public var additionalParameters: [String: Any] {
		switch self {
		case let .getImages(request):
			var parameters = ["lat": request.location.latitude,
							  "lon": request.location.longitude,
			]
			
			if let query = request.query, !query.isEmpty {
				parameters["text"] = query
			}
			
			return parameters
			
		case let .getPhotoInformation(photo):
			return ["photo_id": photo.id,
					"secret": photo.secret]
			
		case let .getPhotoSizes(photo):
			return ["photo_id": photo.id]
		}
	}
	
	public var mockData: Data {
		switch self {
		case .getImages:
			return """
			{
				"photos": {
					"photo": [
						{
							"id": "49682392151",
							"secret": "132d595a83",
							"server": "65535",
							"farm": 66,
							"title": "000385060001_00"
						},
						{
							"id": "49681857893",
							"secret": "630e6a0cff",
							"server": "65535",
							"farm": 66,
							"title": "000385060002_0"
						}
					]
				}
			}
			""".data(using: .utf8)!
			
		case let .getPhotoInformation(photo):
			return """
			{
				"photo": {
					"id": "\(photo.id)",
					"secret": "\(photo.secret)",
					"server": "\(photo.server)",
					"farm": \(photo.farm),
					"dateuploaded": "1584709964",
					"owner": {
						"nsid": "144740425@N02",
						"username": "TomMurray-Smith",
						"realname": "Tom Murray-Smith",
						"iconserver": "65535"
					},
					"dates": {
						"posted": "1584709964",
						"taken": "2020-03-18 11:16:27",
					},
					"location": {
						"latitude": "-33.885012",
						"longitude": "151.196458",
					}
				}
			}
			""".data(using: .utf8)!
		case .getPhotoSizes:
			return """
			{
				"sizes": {
					"size": [
						{
							"label": "Original",
							"width": 3533,
							"height": 5299,
							"url": "http://flickr.com",
						}
					]
				}
			}
			""".data(using: .utf8)!
		}
	}
}
