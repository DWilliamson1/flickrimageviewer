//
//  APIClient.swift
//  Flickr Image Viewer
//
//  Created by Daniel Williamson on 18/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import Foundation

public protocol APIClient {
	var baseURL: URL { get }
	var path: String { get }
	var baseParameters: [String: Any] { get }
	var additionalParameters: [String: Any] { get }
	var mockData: Data { get }
}

