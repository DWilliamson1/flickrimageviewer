//
//  APISession.swift
//  Flickr Image Viewer
//
//  Created by Daniel Williamson on 19/3/20.
//  Copyright © 2020 Daniel WilliamsoadditionalQueryItemsn. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

public protocol APISessionType {
	associatedtype Client: APIClient
		
	init(useMockData: Bool)
}

public final class APISession<Client: APIClient>: APISessionType {
	
	private let useMockData: Bool
	
	public init(useMockData: Bool = false) {
		self.useMockData = useMockData
	}
	
	private let session = URLSession(configuration: .default)
	
	func request<D: Decodable>(_ client: Client, resultObjectType: D.Type) -> Single<D> {
		
		guard let url = URLComponents.fromAPIClient(client)?.url else {
			return Single.error(APIError.invalidParameters)
		}
		
		let request = URLRequest(url: url)
		
		if useMockData {
			return Single.just(client.mockData)
			.map(resultObjectType)
		}
		
		return session.rx.data(request: request)
		.asSingle()
		.map(resultObjectType)
	}
}
