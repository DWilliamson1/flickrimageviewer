//
//  URLComponents+APIClient.swift
//  Flickr Image Viewer
//
//  Created by Daniel Williamson on 19/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import Foundation

extension URLComponents {
	public static func fromAPIClient(_ client: APIClient) -> URLComponents? {
		
		let url = client.baseURL.appendingPathComponent(client.path)
		
		guard var components = URLComponents(string: url.absoluteString) else {
			return nil
		}
		
		let baseQueryItems = queryItemsFromParameters(client.baseParameters)
		let additionalQueryItems = queryItemsFromParameters(client.additionalParameters)
		
		components.queryItems = baseQueryItems + additionalQueryItems
		
		return components
	}
	
	public static func queryItemsFromParameters(_ parameters: [String: Any]) -> [URLQueryItem] {
		return parameters.map { URLQueryItem(name: $0.key, value: "\($0.value)")}
	}
}
