//
//  Single+Decodable.swift
//  Flickr Image Viewer
//
//  Created by Daniel Williamson on 19/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import Foundation
import RxSwift

public extension PrimitiveSequence where Trait == SingleTrait, Element == Data {

	func map<D: Decodable>(_ type: D.Type) -> Single<D> {
		return self.map { try JSONDecoder().decode(type, from: $0) }
	}
}
