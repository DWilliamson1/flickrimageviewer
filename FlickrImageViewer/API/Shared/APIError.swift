//
//  APIError.swift
//  Flickr Image Viewer
//
//  Created by Daniel Williamson on 19/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import Foundation

enum APIError: Error {
	case invalidParameters
	case decoding
}
