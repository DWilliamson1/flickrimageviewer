//
//  LocationManager.swift
//  FlickrImageViewer
//
//  Created by Daniel Williamson on 20/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import Foundation
import CoreLocation
import RxSwift
import RxOptional

public protocol LocationManagerType {
	var currentLocation: Observable<Location?> { get }
	func requestLocation()
}

public final class LocationManager: NSObject, CLLocationManagerDelegate, LocationManagerType {
	
	private let currentLocationRelay = BehaviorSubject<CLLocation?>(value: nil)
	
	public var currentLocation: Observable<Location?> {
		return currentLocationRelay
			.filterNil()
			.map { Location(fromCLLocation: $0) }
			.asObservable()
	}
	
	private let manager = CLLocationManager()
	
	public override init() {
		super.init()
		manager.delegate = self
	}
	
	public func requestLocation() {
		manager.requestWhenInUseAuthorization()
	}
	
	public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
		switch status {
		case .authorizedAlways, .authorizedWhenInUse:
			manager.startUpdatingLocation()
		default:
			break
		}
	}
	
	public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		guard let location = locations.first else {
			return
		}
		
		currentLocationRelay.onNext(location)
	}
}
