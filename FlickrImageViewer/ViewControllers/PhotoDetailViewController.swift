//
//  PhotoDetailViewController.swift
//  FlickrImageViewer
//
//  Created by Daniel Williamson on 21/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import UIKit
import SnapKit
import RxCocoa
import RxSwift

public class PhotoDetailViewController: UIViewController {
	
	private enum Constants {
		static let contentInset = CGFloat(20)
	}
	
	private let disposeBag = DisposeBag()
	
	private let viewModel: PhotoDetailViewModel
	
	private let scrollView: UIScrollView = {
		let scrollView = UIScrollView(frame: .zero)
		
		scrollView.contentInset = UIEdgeInsets(top: 0,
											   left: Constants.contentInset,
											   bottom: 0,
											   right: Constants.contentInset)
		scrollView.backgroundColor = .white
		return scrollView
	}()
	
	private let contentView = UIView()
	
	private let imageView: UIImageView = {
		let imageView = UIImageView(frame: .zero)
		imageView.contentMode = .scaleAspectFill
		imageView.clipsToBounds = true
		return imageView
	}()
	
	private let titleLabel: UILabel = {
		let label = UILabel()
		label.textColor = .black
		label.font = .boldSystemFont(ofSize: 16)
		label.numberOfLines = 0
		return label
	}()
	
	private let userDetailsView: UserDetailsView
	
	private let photoDetailsView: PhotoDetailsView
	
	private let mapView: MapView
	
	public init(photo: Photo) {
		viewModel = PhotoDetailViewModel(flickrAPI: APISession<FlickrAPI>(), photo: photo)
		userDetailsView = UserDetailsView(avatarURL: viewModel.avatarURL.filterNil(),
										  username: viewModel.username,
										  realName: viewModel.realName)
		
		photoDetailsView = PhotoDetailsView(imageSize: viewModel.imageSize,
											uploadDate: viewModel.imageUploadDate.filterNil())
		
		mapView = MapView(location: viewModel.imageLocation)
		
		super.init(nibName: nil, bundle: nil)
		
		setup()
	}
	
	required init?(coder: NSCoder) {
		viewModel = PhotoDetailViewModel()
		userDetailsView = UserDetailsView(avatarURL: viewModel.avatarURL.filterNil(),
										  username: viewModel.username,
										  realName: viewModel.realName)
		
		photoDetailsView = PhotoDetailsView(imageSize: viewModel.imageSize,
											uploadDate: viewModel.imageUploadDate.filterNil())
		
		mapView = MapView(location: viewModel.imageLocation)
		
		
		super.init(coder: coder)
	}
	
	private func setup() {
		
		title = "Photo Detail"
		navigationItem.largeTitleDisplayMode = .never
		navigationController?.navigationBar.prefersLargeTitles = true
		
		view.addSubview(scrollView)
		scrollView.snp.makeConstraints { make in
			make.top.equalToSuperview()
			make.left.bottom.right.equalToSuperview()
		}
		
		scrollView.addSubview(contentView)
		contentView.snp.makeConstraints { make in
			make.top.bottom.equalTo(scrollView)
			make.left.right.equalTo(view)
		}
		
		contentView.addSubview(imageView)
		imageView.snp.makeConstraints { make in
			make.top.equalToSuperview()
			make.width.equalToSuperview()
			make.height.equalTo(imageView.snp.width).dividedBy(1.2)
		}
		
		let containerView = UIView()
		contentView.addSubview(containerView)
		containerView.snp.makeConstraints { make in
			make.top.equalTo(imageView.snp.bottom).offset(Constants.contentInset)
			make.left.equalToSuperview().offset(Constants.contentInset)
			make.right.equalToSuperview().offset(-Constants.contentInset)
			make.bottom.equalToSuperview()
		}
		
		containerView.addSubview(titleLabel)
		titleLabel.snp.makeConstraints { make in
			make.top.equalToSuperview()
			make.width.equalToSuperview()
		}
		
		containerView.addSubview(userDetailsView)
		userDetailsView.snp.makeConstraints { make in
			make.top.equalTo(titleLabel.snp.bottom).offset(Constants.contentInset)
			make.width.equalToSuperview()
		}
		
		containerView.addSubview(photoDetailsView)
		photoDetailsView.snp.makeConstraints { make in
			make.top.equalTo(userDetailsView.snp.bottom).offset(Constants.contentInset * 2)
			make.width.equalToSuperview()
		}
		
		containerView.addSubview(mapView)
		mapView.snp.makeConstraints { make in
			make.top.equalTo(photoDetailsView.snp.bottom).offset(Constants.contentInset * 2)
			make.height.equalTo(mapView.snp.width).dividedBy(2)
			make.width.equalToSuperview()
			make.bottom.equalToSuperview().offset(-Constants.contentInset)
		}
		
		setupRx()
	}
	
	private func setupRx() {
		viewModel.imageURL
			.filterNil()
			.subscribe(onNext: { [weak imageView] url in
				_ = imageView?.loadImage(fromURL: url)
			})
			.disposed(by: disposeBag)
		
		viewModel.imageName
			.bind(to: titleLabel.rx.text)
			.disposed(by: disposeBag)
	}
}
