//
//  HomeScreenViewController.swift
//  FlickrImageViewer
//
//  Created by Daniel Williamson on 21/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import UIKit
import SnapKit
import RxCocoa
import RxSwift

public class HomeScreenViewController: UIViewController {
	
	private enum Constants {
		static let cellSpacing = CGFloat(5)
		static let paginationOffset = CGFloat(10)
	}
	
	private let disposeBag = DisposeBag()
	private let viewModel: HomeScreenViewModel
	
	private let collectionView: UICollectionView = {
		let layout = UICollectionViewFlowLayout()
		let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
		collectionView.backgroundColor = .white
		collectionView.register(HomeScreenCollectionViewCell.self,
								forCellWithReuseIdentifier: HomeScreenCollectionViewCell.reuseIdentifier)
		collectionView.allowsSelection = true
		return collectionView
	}()
	
	private let searchController: PhotosSearchViewController = {
		let controller = PhotosSearchViewController(searchResultsController: nil)
		controller.hidesNavigationBarDuringPresentation = true
		controller.obscuresBackgroundDuringPresentation = false
		return controller
	}()
	
	public init() {
		
		let viewModelInputs = HomeScreenViewModelInputs(searchQuery: searchController.searchQuery,
														loadNextPage: collectionView.rx.contentOffset
															.filterShowNextPage(scrollView: collectionView, offset: Constants.paginationOffset)
															.throttle(RxTimeInterval.seconds(2), latest: false, scheduler: MainScheduler.instance))
		
		viewModel = HomeScreenViewModel(flickrAPI: APISession<FlickrAPI>(),
										locationManager: LocationManager(),
										inputs: viewModelInputs)
		
		super.init(nibName: nil, bundle: nil)
		setup()
	}
	
	required init?(coder: NSCoder) {
		viewModel = HomeScreenViewModel()
		super.init(coder: coder)
	}
	
	override public func viewDidLoad() {
		super.viewDidLoad()
		
		title = "Recent Photos"
		definesPresentationContext = true
		setupNavigationBar()
	}
	
	override public func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		setupNavigationBar()
	}
	
	private func setup() {
		setupCollectionView()
		setupSearch()
	}
	
	private func setupNavigationBar() {
		navigationItem.largeTitleDisplayMode = .always
		
		navigationController?.navigationBar.prefersLargeTitles = true
		navigationController?.navigationBar.sizeToFit()
	}
	
	private func setupCollectionView() {
		view.addSubview(collectionView)
		collectionView.snp.makeConstraints { $0.edges.equalToSuperview() }
		
		func setupCell(photo: Photo, cell: HomeScreenCollectionViewCell) {
			guard let imageURL = photo.buildImageURLSource(imageSize: .small) else {
				return
			}
			
			cell.setImageURL(imageURL)
		}
		
		let photos = viewModel.photos.share()
		
		photos
			.bind(to: collectionView.rx.items(cellIdentifier: HomeScreenCollectionViewCell.reuseIdentifier,
											  cellType: HomeScreenCollectionViewCell.self)) {
												setupCell(photo: $1, cell: $2) }
			.disposed(by: disposeBag)
		
		Observable.combineLatest(photos, viewModel.isFetching)
			.map { return $0.isEmpty && !$1 }
			.observeOn(MainScheduler.instance)
			.subscribe(onNext: { [weak collectionView] shouldShowEmptyView in
				collectionView?.backgroundView = shouldShowEmptyView ? EmptyStateView() : nil
			})
			.disposed(by: disposeBag)
		
		collectionView.rx.modelSelected(Photo.self)
			.asDriver()
			.drive(onNext: { [weak self] photo in
				self?.pushPhotoDetailViewController(withPhoto: photo)
			})
			.disposed(by: disposeBag)
		
		collectionView.rx.setDelegate(self)
			.disposed(by: disposeBag)
	}
	
	private func setupSearch() {
		navigationItem.searchController = searchController
		searchController.setFetchingData(viewModel.isFetching)
	}
	
	private func pushPhotoDetailViewController(withPhoto photo: Photo) {
		let photoDetailViewController = PhotoDetailViewController(photo: photo)
		navigationController?.pushViewController(photoDetailViewController, animated: true)
	}
}

extension HomeScreenViewController: UICollectionViewDelegateFlowLayout {
	
	public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
		return Constants.cellSpacing
	}
	
	public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		let size = collectionView.frame.width / 3 - Constants.cellSpacing
		return CGSize(width: size, height: size)
	}
}

