//
//  PhotosSearchViewController.swift
//  FlickrImageViewer
//
//  Created by Daniel Williamson on 21/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import UIKit
import RxSwift

public class PhotosSearchViewController: UISearchController {
	
	private let disposeBag = DisposeBag()
	
	private let loadingIndicator: UIActivityIndicatorView = {
		let indicator = UIActivityIndicatorView(style: .medium)
		indicator.isHidden = true
		indicator.startAnimating()
		return indicator
	}()
	
	public func setFetchingData(_ fetchingData: Observable<Bool>) {
		
		fetchingData
			.map { !$0 }
			.bind(to: loadingIndicator.rx.isHidden)
			.disposed(by: disposeBag)
	}
	
	override public func viewDidLoad() {
		super.viewDidLoad()
		searchBar.searchTextField.rightView = loadingIndicator
		searchBar.searchTextField.rightViewMode = .always
		searchBar.searchTextField.clearButtonMode = .never
	}
	
	public var searchQuery: Observable<String?> {
		return searchBar.rx.text
			.debounce(RxTimeInterval.seconds(1), scheduler: MainScheduler.instance)
	}
}
