//
//  Obsfucator.swift
//  Flickr Image Viewer
//
//  Created by Daniel Williamson on 19/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import Foundation

public enum Obfuscator {
    
    private static var salt: String = "\(String(describing: Obfuscator.self))\(String(describing: NSObject.self))"
    
    static func setSalt(_ salt: String) {
        Obfuscator.salt = salt
    }
	
    internal static func bytesByObfuscatingString(string: String) -> [UInt8] {
        let text = [UInt8](string.utf8)
        let cipher = [UInt8](salt.utf8)
        let length = cipher.count
        
        var encrypted = [UInt8]()
        
        for character in text.enumerated() {
            encrypted.append(character.element ^ cipher[character.offset % length])
        }
        
        return encrypted
    }
    
    public static func reveal(key: [UInt8]) -> String {
        let cipher = [UInt8](salt.utf8)
        let length = cipher.count
        
        var decrypted = [UInt8]()
        
        for value in key.enumerated() {
            decrypted.append(value.element ^ cipher[value.offset % length])
        }
        
        return String(bytes: decrypted, encoding: .utf8)!
    }
}
