//
//  HomeScreenCollectionViewCell.swift
//  FlickrImageViewer
//
//  Created by Daniel Williamson on 21/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import UIKit
import SnapKit

public protocol HomeScreenCollectionViewCellType {
	func setImageURL(_ url: URL)
}

public class HomeScreenCollectionViewCell: UICollectionViewCell, ReusableView, HomeScreenCollectionViewCellType {
	
	private var imageURL: URL?
	private var imageTask: URLSessionTask?
	
	private let thumbnailImageView: UIImageView = {
		let imageView = UIImageView(frame: .zero)
		imageView.contentMode = .scaleAspectFill
		return imageView
	}()
    
	override init(frame: CGRect) {
		super.init(frame: .zero)
		setup()
	}
	
	required init?(coder: NSCoder) {
		super.init(coder: coder)
	}
	
	private func setup() {
		
		contentView.clipsToBounds = true
		contentView.addSubview(thumbnailImageView)
		thumbnailImageView.snp.makeConstraints { $0.edges.equalToSuperview() }
	}
	
	public func setImageURL(_ url: URL) {
		imageTask = thumbnailImageView.loadImage(fromURL: url)
	}
	
	override public func prepareForReuse() {
		super.prepareForReuse()
		
		if let imageTask = imageTask, imageTask.state != URLSessionTask.State.canceling {
			imageTask.cancel()
		}
		
		thumbnailImageView.image = nil
	}
}
