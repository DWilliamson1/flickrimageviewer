//
//  MapView.swift
//  FlickrImageViewer
//
//  Created by Daniel Williamson on 22/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import UIKit
import MapKit
import RxSwift

public class MapView: UIView {
	
	private enum Constants {
		static let annotationReuseIdentifier = "annotation"
	}
	
	private let disposeBag = DisposeBag()
	
	private let location: Observable<Location>
	
	private let mapView: MKMapView = {
		let mapView = MKMapView(frame: .zero)
		mapView.isHidden = true
		mapView.isUserInteractionEnabled = false
		return mapView
	}()
	
	public init(location: Observable<Location>) {
		self.location = location
		super.init(frame: .zero)
		
		setup()
	}
	
	required init?(coder: NSCoder) {
		location = Observable.empty()
		super.init(coder: coder)
	}
	
	private func setup() {
		
		mapView.delegate = self
		
		addSubview(mapView)
		mapView.snp.makeConstraints { $0.edges.equalToSuperview() }
		
		location
			.observeOn(MainScheduler.instance)
			.do(onNext: { [weak mapView] _ in
				mapView?.isHidden = false
			})
			.subscribe(onNext: { [weak self] location in
				self?.showLocation(location)
			})
			.disposed(by: disposeBag)
	}
	
	private func showLocation(_ location: Location) {
		
		guard let coreLocation = location.toCLLocation() else {
			return
		}
		
		let point = MKPointAnnotation()
		point.coordinate = coreLocation.coordinate
		mapView.addAnnotation(point)
		
		mapView.showAnnotations([point], animated: false)
	}
}

extension MapView: MKMapViewDelegate {
	public func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
		guard annotation is MKPointAnnotation else { return nil }
		
		var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: Constants.annotationReuseIdentifier)
		
		if let annotationView = annotationView {
			annotationView.annotation = annotation
		} else {
			annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: Constants.annotationReuseIdentifier)
		}
		
		return annotationView
	}
}
