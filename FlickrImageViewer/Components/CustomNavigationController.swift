//
//  CustomNavigationController.swift
//  FlickrImageViewer
//
//  Created by Daniel Williamson on 22/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import UIKit

public class CustomNavigationController: UINavigationController {
	public override init(rootViewController: UIViewController) {
		super.init(rootViewController: rootViewController)
		setup()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	private func setup(){
		navigationBar.tintColor = .black
	}
	
	public override func pushViewController(_ viewController: UIViewController, animated: Bool) {
		viewController.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
		
		super.pushViewController(viewController, animated: true)
	}
}
