//
//  ReusableView.swift
//  FlickrImageViewer
//
//  Created by Daniel Williamson on 21/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import Foundation
import UIKit

public protocol ReusableView: class {}

extension ReusableView where Self: UIView {

	public static var reuseIdentifier: String {
		return String(describing: self)
	}
}
