//
//  PhotoDetailsView.swift
//  FlickrImageViewer
//
//  Created by Daniel Williamson on 22/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

private enum PhotoDetail: String {
	case size = "Size"
	case uploadDate = "Upload Date"
}

public class PhotoDetailsView: UIView {
	
	private let disposeBag = DisposeBag()
	
	private let imageSize: Observable<String>
	private let uploadDate: Observable<String>
	
	private let sizeValueLabel: UILabel = {
		let label = UILabel()
		label.textColor = .black
		label.font = .systemFont(ofSize: 16)
		label.numberOfLines = 1
		label.textAlignment = .center
		return label
	}()
	
	private let uploadDateValueLabel: UILabel = {
		let label = UILabel()
		label.textColor = .black
		label.font = .systemFont(ofSize: 16)
		label.numberOfLines = 1
		label.textAlignment = .center
		return label
	}()
	
	private let containerStackView: UIStackView = {
		let stackView = UIStackView(frame: .zero)
		stackView.axis = .horizontal
		stackView.distribution = .fillEqually
		stackView.isHidden = true
		return stackView
	}()
	
	public init(imageSize: Observable<PhotoSize?>, uploadDate: Observable<String>) {
		self.imageSize = imageSize
			.map({ size in
				guard let size = size else {
					return "N/A"
				}
				return "\(size.width) x \(size.height)"
			})
		
		self.uploadDate = uploadDate
		
		super.init(frame: .zero)
		
		setup()
	}
	
	required init?(coder: NSCoder) {
		imageSize = Observable.empty()
		uploadDate = Observable.empty()
		super.init(coder: coder)
	}
	
	private func setup() {
		
		let sizeView = viewForDetail(.size, valueLabel: sizeValueLabel)
		let uploadDateView = viewForDetail(.uploadDate, valueLabel: uploadDateValueLabel)
		
		containerStackView.addArrangedSubview(sizeView)
		containerStackView.addArrangedSubview(uploadDateView)
		
		addSubview(containerStackView)
		containerStackView.snp.makeConstraints { make in
			make.edges.equalToSuperview()
		}
		
		setupRx()
	}
	
	private func viewForDetail(_ detail: PhotoDetail, valueLabel: UILabel) -> UIView {
		
		let titleLabel = UILabel()
		titleLabel.textColor = .gray
		titleLabel.font = .systemFont(ofSize: 14)
		titleLabel.numberOfLines = 1
		titleLabel.text = detail.rawValue
		titleLabel.textAlignment = .center
		
		let stackView = UIStackView(arrangedSubviews: [titleLabel, valueLabel])
		stackView.axis = .vertical
		stackView.spacing = 6
		stackView.alignment = .center
		
		return stackView
	}
	
	private func setupRx() {
		Observable.zip(imageSize, uploadDate)
			.observeOn(MainScheduler.instance)
			.subscribe(onNext: { [weak self] imageSize, uploadDate in
				guard let self = self else {
					return
				}
				
				self.containerStackView.isHidden = false
				self.sizeValueLabel.text = imageSize
				self.uploadDateValueLabel.text = uploadDate
				
			})
			.disposed(by: disposeBag)
		
	}
}
