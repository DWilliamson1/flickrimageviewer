//
//  UserDetailsView.swift
//  FlickrImageViewer
//
//  Created by Daniel Williamson on 22/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

public class UserDetailsView: UIView {
	
	private enum Constants {
		static let avatarSize = CGFloat(48)
	}
	
	private let disposeBag = DisposeBag()
	
	private let avatarURL: Observable<URL>
	private let username: Observable<String>
	private let realName: Observable<String>
	
	private let avatarImageView: UIImageView = {
		let imageView = UIImageView(frame: .zero)
		imageView.contentMode = .scaleAspectFill
		return imageView
	}()
	
	private let realNameLabel: UILabel = {
		let label = UILabel()
		label.textColor = .black
		label.font = .systemFont(ofSize: 16)
		label.numberOfLines = 0
		return label
	}()
	
	private let usernameLabel: UILabel = {
		let label = UILabel()
		label.textColor = .gray
		label.font = .systemFont(ofSize: 14)
		label.numberOfLines = 0
		return label
	}()
	
	public init(avatarURL: Observable<URL>, username: Observable<String>, realName: Observable<String>) {
		
		self.avatarURL = avatarURL
		self.username = username
		self.realName = realName
		
		super.init(frame: .zero)
		
		setup()
	}
	
	required init?(coder: NSCoder) {
		avatarURL = Observable.empty()
		username = Observable.empty()
		realName = Observable.empty()
		super.init(coder: coder)
	}
	
	private func setup() {
		
		addSubview(avatarImageView)
		avatarImageView.snp.makeConstraints { make in
			make.top.left.equalToSuperview()
			make.size.equalTo(Constants.avatarSize)
		}
		
		let stackView = UIStackView(arrangedSubviews: [realNameLabel, usernameLabel])
		stackView.axis = .vertical
		
		addSubview(stackView)
		stackView.snp.makeConstraints { make in
			make.left.equalTo(avatarImageView.snp.right).offset(10)
			make.centerY.equalTo(avatarImageView)
			make.bottom.equalToSuperview()
		}
		
		setupRx()
	}
	
	private func setupRx() {
		avatarURL
			.subscribe(onNext: { [weak avatarImageView] url in
				_ = avatarImageView?.loadImage(fromURL: url)
			})
			.disposed(by: disposeBag)
		
		username.bind(to: usernameLabel.rx.text)
			.disposed(by: disposeBag)
		
		realName.bind(to: realNameLabel.rx.text)
			.disposed(by: disposeBag)
	}
}
