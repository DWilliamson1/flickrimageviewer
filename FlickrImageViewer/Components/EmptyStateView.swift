//
//  EmptyStateView.swift
//  FlickrImageViewer
//
//  Created by Daniel Williamson on 22/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import UIKit
import SnapKit

public class EmptyStateView: UIView {
	
	private enum Constants {
		static let spacing = CGFloat(20)
	}
	
	private let titleLabel: UILabel = {
		let label = UILabel()
		label.textColor = .black
		label.font = .boldSystemFont(ofSize: 16)
		label.text = "No Results Found"
		label.textAlignment = .center
		return label
	}()
	
	private let subtitleLabel: UILabel = {
		let label = UILabel()
		label.textColor = .gray
		label.font = .systemFont(ofSize: 14)
		label.text = "Please try changing your search query"
		label.textAlignment = .center
		return label
	}()
	
	public override init(frame: CGRect) {
		super.init(frame: frame)
		setup()
	}
	
	required init?(coder: NSCoder) {
		super.init(coder: coder)
	}
	
	private func setup() {
		let stackView = UIStackView(arrangedSubviews: [titleLabel, subtitleLabel])
		stackView.axis = .vertical
		
		addSubview(stackView)
		
		stackView.snp.makeConstraints { make in
			make.center.equalToSuperview()
			make.left.equalToSuperview().offset(Constants.spacing)
			make.right.equalToSuperview().offset(-Constants.spacing)
		}
	}
}
