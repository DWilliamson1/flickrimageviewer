//
//  UIScrollView+Extensions.swift
//  FlickrImageViewer
//
//  Created by Daniel Williamson on 22/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import RxSwift
import UIKit

extension ObservableType where Element == CGPoint {
	
	public func filterShowNextPage(scrollView: UIScrollView, offset: CGFloat) -> Observable<Void> {
		return self.filter { contentOffset in
			let yOffset = contentOffset.y
			
			guard yOffset > 0 else {
				return false
			}
			
			let contentHeight = scrollView.contentSize.height + offset
			if yOffset > contentHeight - scrollView.frame.size.height {
				return true
			}
			return false
		}
		.map { _ in }
	}
}
