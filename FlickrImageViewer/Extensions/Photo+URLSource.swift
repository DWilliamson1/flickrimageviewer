//
//  Photo+URLSource.swift
//  FlickrImageViewer
//
//  Created by Daniel Williamson on 20/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import Foundation

extension Photo {
	public func buildImageURLSource(imageSize: ImageSize) -> URL? {
		var components = URLComponents()
		components.scheme = "https"
		components.host = "farm\(farm).staticflickr.com"
		components.path = "/\(server)/\(id)_\(secret)_\(imageSize.rawValue).jpg"
		
		return components.url
	}
}

extension PhotoInfo {
	public func buildAvatarURLSource() -> URL? {
		var components = URLComponents()
		components.scheme = "https"
		components.host = "farm\(farm).staticflickr.com"
		components.path = "/\(owner.iconServer)/buddyicons/\(owner.nsid).jpg"
		
		return components.url
	}
}
