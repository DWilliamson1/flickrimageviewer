//
//  UIImageView+URL.swift
//  FlickrImageViewer
//
//  Created by Daniel Williamson on 21/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
	
	/**
	Loads an Image From a URL
	
	First it checks the URLCache,
	if the previous request does not exist then download the image
	from the URL using URLSession
	
	*/
	public func loadImage(fromURL url: URL) -> URLSessionTask? {
		
		let cache = URLCache.shared
		let request = URLRequest(url: url)
		
		func setImage(_ image: UIImage) {
			
			DispatchQueue.main.async {
				UIView.transition(with: self, duration: 0.1, options: [.transitionCrossDissolve], animations: {
					self.image = image
				})
			}
		}
		
		if let data = cache.cachedResponse(for: request)?.data, let image = UIImage(data: data) {
			setImage(image)
		} else {
			
			let task = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
				guard let data = data, let response = response, let image = UIImage(data: data) else {
					return
				}
				
				let cachedData = CachedURLResponse(response: response, data: data)
				cache.storeCachedResponse(cachedData, for: request)
				
				setImage(image)
				
			})
			
			task.resume()
			return task
		}
		
		return nil
	}
}
