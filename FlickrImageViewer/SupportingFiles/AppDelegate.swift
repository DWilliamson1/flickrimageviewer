//
//  AppDelegate.swift
//  Flickr Image Viewer
//
//  Created by Daniel Williamson on 18/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	
	private enum Constants {
		static let cacheSize = 1024
		static let memoryMB = 4
		static let diskMB = 20
	}
	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
		URLCache.shared = URLCache(memoryCapacity: Constants.memoryMB * Constants.cacheSize * Constants.cacheSize,
								   diskCapacity: Constants.diskMB * Constants.cacheSize * Constants.cacheSize)
		return true
	}
}

