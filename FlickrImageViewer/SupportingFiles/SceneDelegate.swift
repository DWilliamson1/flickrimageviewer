//
//  SceneDelegate.swift
//  Flickr Image Viewer
//
//  Created by Daniel Williamson on 18/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

	var window: UIWindow?

	func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
		
		guard let windowScene = (scene as? UIWindowScene) else {
			return
		}
		
		window = UIWindow(frame: windowScene.coordinateSpace.bounds)
		window?.windowScene = windowScene
		window?.rootViewController = CustomNavigationController(rootViewController: HomeScreenViewController())
		window?.makeKeyAndVisible()
	}
}

