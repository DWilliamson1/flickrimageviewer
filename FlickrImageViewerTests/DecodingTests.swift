//
//  DecodingTests.swift
//  FlickrImageViewerTests
//
//  Created by Daniel Williamson on 23/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import Foundation
import XCTest

class DecodingTests: XCTestCase {
	
	var jsonDecoder: JSONDecoder!
	
	override func setUp() {
		jsonDecoder = JSONDecoder()
	}
	
	func testPhotoDecoding() {
		let id = "49682392151"
		let secret = "132d595a83"
		let server = "65535"
		let farm = 66
		let title = "000385060001_00"
		
		let expected = "\(id) - \(secret) - \(server) - \(farm) - \(title)"
		
		let request = PhotosRequestModel(location: Location(latitude: "", longitude: ""),
										 query: "",
										 limit: 20,
										 offset: 0)
		
		let mockData = FlickrAPI.getImages(request).mockData
		
		guard let photos = try? jsonDecoder.decode(PhotosResponseModel.self, from: mockData).photos else {
			XCTFail("Failed to decode photos")
			return
		}
		
		guard let photo = photos.first else {
			XCTFail("Cannot find first photo")
			return
		}
		
		XCTAssertEqual(expected, photo.combinedData())
	}
	
	func testPhotoInfoDecoding() {
		
		let nsid = "144740425@N02"
		let iconServer = "65535"
		let realName = "Tom Murray-Smith"
		let username = "TomMurray-Smith"
		let datePosted = "1584709964"
		let dateTaken = "2020-03-18 11:16:27"
		let lat = "-33.885012"
		let long = "151.196458"

		let expected = "\(nsid) - \(iconServer) - \(realName) - \(username) - \(datePosted) - \(dateTaken) - \(lat) - \(long)"

		let mockPhoto = Photo(id: "", secret: "", server: "", farm: 0, title: "")
		
		let mockData = FlickrAPI.getPhotoInformation(mockPhoto).mockData
		
		guard let photoInformation = try? jsonDecoder.decode(PhotoInfoResponseModel.self, from: mockData).photo else {
			XCTFail("Failed to decode photo info")
			return
		}
		
		XCTAssertEqual(expected, photoInformation.combinedData())
	}
	
	func testPhotoSizesDecoding() {
		
		let label = "Original"
		let width = 3533
		let height = 5299
		let url = "http://flickr.com"
		
		let expected = "\(label) - \(width) - \(height) - \(url)"

		let mockPhoto = Photo(id: "", secret: "", server: "", farm: 0, title: "")
		
		let mockData = FlickrAPI.getPhotoSizes(mockPhoto).mockData
		
		guard let photoSizes = try? jsonDecoder.decode(PhotoSizesResponseModel.self, from: mockData).sizes else {
			XCTFail("Failed to decode photo sizes")
			return
		}
		
		guard let size = photoSizes.first else {
			XCTFail("Cannot find size")
			return
		}
		
		XCTAssertEqual(expected, size.combinedData())
	}
}

private extension Photo {
	func combinedData() -> String {
		return "\(id) - \(secret) - \(server) - \(farm) - \(title)"
	}
}

private extension PhotoInfo {
	func combinedData() -> String {
		return "\(owner.nsid) - \(owner.iconServer) - \(owner.realName) - \(owner.username) - \(dates.posted) - \(dates.taken) - \(location.latitude) - \(location.longitude)"
	}
}

private extension PhotoSize {
	func combinedData() -> String {
		return "\(label) - \(width) - \(height) - \(url)"
	}
}
