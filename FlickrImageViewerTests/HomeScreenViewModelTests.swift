//
//  HomeScreenViewModelTests.swift
//  FlickrImageViewerTests
//
//  Created by Daniel Williamson on 23/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import XCTest
import RxTest
import RxSwift

class HomeScreenViewModelTests: XCTestCase {
	
	private var disposeBag = DisposeBag()
	
	private var flickrAPI: APISession<FlickrAPI>!
	private var locationManager: LocationManagerType!
	
	override func setUp() {
		flickrAPI = APISession<FlickrAPI>(useMockData: true)
		locationManager = MockLocationManager(latitude: "-33.885012", longitude: "151.196458")
		disposeBag = DisposeBag()
	}
	
	func testIsFetching() {
		
		let scheduler = TestScheduler(initialClock: 0)
		let observer = scheduler.createObserver(String.self)
		
		let expected = "false, true, false, true, false"
		
		let searchQuery = BehaviorSubject<String?>(value: nil)
		let inputs = HomeScreenViewModelInputs(searchQuery: searchQuery, loadNextPage: Observable.empty())
		
		let viewModel = HomeScreenViewModel(flickrAPI: flickrAPI, locationManager: locationManager, inputs: inputs)
		
		viewModel.isFetching
			.map { $0 ? "true" : "false" }
			.subscribe(observer)
			.disposed(by: disposeBag)
		
		viewModel.photos
			.subscribe()
			.disposed(by: disposeBag)
		
		scheduler.start()
		
		searchQuery.onNext("Searching")
		locationManager.requestLocation()
		
		let events = observer.events.compactMap { $0.value.element }.joined(separator: ", ")
		
		XCTAssertEqual(expected, events)
	}
}

private class MockLocationManager: LocationManagerType {
	
	private let latitude: String
	private let longitude: String
	
	init(latitude: String, longitude: String) {
		self.latitude = latitude
		self.longitude = longitude
	}
	
	var currentLocation: Observable<Location?> {
		return currentLocationSubject.asObservable()
	}
	
	private let currentLocationSubject = BehaviorSubject<Location?>(value: nil)
	
	func requestLocation() {
		currentLocationSubject.onNext(Location(latitude: latitude, longitude: longitude))
	}
}
