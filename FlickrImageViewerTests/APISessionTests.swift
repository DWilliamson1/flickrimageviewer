//
//  APISessionTests.swift
//  Flickr Image ViewerTests
//
//  Created by Daniel Williamson on 19/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import XCTest
import RxSwift
import RxTest

class APISessionTests: XCTestCase {
	
	private var disposeBag = DisposeBag()
	
	override func setUp() {
		disposeBag = DisposeBag()
	}

	func testRequest() {
		let mockSession = APISession<MockAPI>(useMockData: true)
		let scheduler = TestScheduler(initialClock: 0)
		let observer = scheduler.createObserver([MockResult].self)
		
		mockSession.request(.getMockResults(param1: "PARAM1", param2: 242), resultObjectType: [MockResult].self)
		.asObservable()
		.subscribe(observer)
		.disposed(by: disposeBag)
		
		scheduler.start()
		
		guard let results = observer.events[0].value.element else {
			return XCTFail("No Elements")
		}
		
		guard results.count == 3 else {
			return XCTFail("Incorrect number of elements")
		}
		
		let firstResult = results[0]
		
		guard firstResult.id == 1, firstResult.name == "result1" else {
			return XCTFail("Incorrect values")
		}
	}
}
