//
//  MockAPI.swift
//  FlickrImageViewerTests
//
//  Created by Daniel Williamson on 19/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import Foundation

struct MockResult: Decodable {
	let id: Int
	let name: String
}

enum MockAPI {
	case getMockResults(param1: String, param2: Int)
}

extension MockAPI: APIClient {
	
	private enum Constants {
		static let url = "www.testURL.com"
		static let path = "/mock/"
		static let baseParameters = ["api_key": "APIKEY"]
	}

	var baseURL: URL {
		return URL(string: Constants.url)!
	}
	
	var path: String {
		return Constants.path
	}
	
	var baseParameters: [String: Any] {
		return Constants.baseParameters
	}
	
	var additionalParameters: [String: Any] {
		if case let .getMockResults(param1, param2) = self {
			return ["param1": param1,
					"param2": param2]
		}
		
		return [:]
	}
	
	var mockData: Data {
		return """
		[
			{
				"id": 1,
				"name": "result1"
			},
			{
				"id": 2,
				"name": "result2"
			},
			{
				"id": 3,
				"name": "result3"
			}
		]
		""".data(using: .utf8)!
	}
}
