//
//  URLComponentsTests.swift
//  FlickrImageViewerTests
//
//  Created by Daniel Williamson on 19/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import XCTest

class URLComponentsTests: XCTestCase {

	func testFromAPIClient() {
		
		let param1 = "PARAM1"
		let param2 = 5
		
		let expectedAPIKey = "api_key: APIKEY"
		let expectedParam1 = "param1: PARAM1"
		let expectedParam2 = "param2: 5"

		let client = MockAPI.getMockResults(param1: param1, param2: param2)
		 
		guard let components = URLComponents.fromAPIClient(client), let queryComponents = components.queryItems else {
			return XCTFail("Failed to initialize components")
		}
				
		let queryComponentsString = queryComponents.map { "\($0.name): \($0.value ?? "")" }.joined(separator: ",")
		
		XCTAssertTrue(queryComponentsString.contains(expectedAPIKey), "Query does not contain API Key")
		XCTAssertTrue(queryComponentsString.contains(expectedParam1), "Query does not contain Param1")
		XCTAssertTrue(queryComponentsString.contains(expectedParam2), "Query does not contain Param2")
	}
}
