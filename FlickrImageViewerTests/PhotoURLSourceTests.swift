//
//  PhotoURLSourceTests.swift
//  FlickrImageViewerTests
//
//  Created by Daniel Williamson on 22/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import XCTest

class PhotoURLSourceTests: XCTestCase {

	func testBuildImageURLSource() {
		let id = "ID"
		let secret = "SECRET"
		let server = "SERVER"
		let farm = 5
		let imageSize = ImageSize.large
		
		let expectedURLString = "https://farm\(farm).staticflickr.com/\(server)/\(id)_\(secret)_\(imageSize.rawValue).jpg"
		
		let mockPhoto = Photo(id: id, secret: secret, server: server, farm: farm, title: "")
		
		guard let url = mockPhoto.buildImageURLSource(imageSize: imageSize) else {
			XCTFail("Failed to build URL")
			return
		}
		
		XCTAssertEqual(expectedURLString, url.absoluteString)
	}
	
	func testBuildAvatarURLSource() {
		let nsid = "NSID"
		let iconServer = "ICONSERVER"
		let farm = 5
		
		let owner = PhotoOwner(nsid: nsid, username: "", realName: "", iconServer: iconServer)
		
		let expectedURLString = "https://farm\(farm).staticflickr.com/\(iconServer)/buddyicons/\(nsid).jpg"
		
		let mockPhotInfo = PhotoInfo(farm: farm, owner: owner)
		
		guard let url = mockPhotInfo.buildAvatarURLSource() else {
			XCTFail("Failed to build URL")
			return
		}
		
		XCTAssertEqual(expectedURLString, url.absoluteString)
	}
}
