//
//  PhotoDetailViewModelTests.swift
//  FlickrImageViewerTests
//
//  Created by Daniel Williamson on 23/3/20.
//  Copyright © 2020 Daniel Williamson. All rights reserved.
//

import Foundation
import XCTest
import RxTest
import RxSwift
import RxOptional

class PhotoDetailViewModelTests: XCTestCase {
	private var disposeBag = DisposeBag()
	
	private var flickrAPI: APISession<FlickrAPI>!
	
	override func setUp() {
		flickrAPI = APISession<FlickrAPI>(useMockData: true)
		disposeBag = DisposeBag()
	}
	
	func testPhotoObservables() {
		let scheduler = TestScheduler(initialClock: 0)
		let observer = scheduler.createObserver(String.self)
		let title = "TITLE"
		
		let expected = "TITLE - https://farm0.staticflickr.com//__b.jpg"
		
		let mockPhoto = Photo(id: "", secret: "", server: "", farm: 0, title: title)
		let viewModel = PhotoDetailViewModel(flickrAPI: flickrAPI, photo: mockPhoto)
		
		Observable.combineLatest(viewModel.imageName, viewModel.imageURL.filterNil(), resultSelector: {
			return "\($0) - \($1.absoluteString)"
		})
			.subscribe(observer)
			.disposed(by: disposeBag)
		
		scheduler.start()
		
		guard let result = observer.events[0].value.element else {
			return XCTFail("No Element")
		}
		
		XCTAssertEqual(expected, result)
	}
	
	func testPhotoInfoObservables() {
		let scheduler = TestScheduler(initialClock: 0)
		let observer = scheduler.createObserver(String.self)
		
		let expected = "Mar 21, 2020 - -33.885012 151.196458 - https://farm0.staticflickr.com/65535/buddyicons/144740425@N02.jpg - @TomMurray-Smith, - Tom Murray-Smith"
		
		let mockPhoto = Photo()
		let viewModel = PhotoDetailViewModel(flickrAPI: flickrAPI, photo: mockPhoto)
		
		Observable.combineLatest(viewModel.imageUploadDate.filterNil(), viewModel.imageLocation, viewModel.avatarURL.filterNil(), viewModel.username, viewModel.realName, resultSelector: {
			return "\($0) - \($1.latitude) \($1.longitude) - \($2.absoluteString) - \($3), - \($4)"
		})
			.subscribe(observer)
			.disposed(by: disposeBag)
		
		scheduler.start()
		
		guard let result = observer.events[0].value.element else {
			return XCTFail("No Element")
		}
		
		XCTAssertEqual(expected, result)
	}
	
	func testImageSize() {
		let scheduler = TestScheduler(initialClock: 0)
		let observer = scheduler.createObserver(String.self)
		
		let expected = "width: 3533 - height: 5299"
		
		let mockPhoto = Photo()
		let viewModel = PhotoDetailViewModel(flickrAPI: flickrAPI, photo: mockPhoto)
		
		viewModel.imageSize
		.filterNil()
		.map { "width: \($0.width) - height: \($0.height)" }
		.subscribe(observer)
		.disposed(by: disposeBag)
		
		scheduler.start()
		
		guard let result = observer.events[0].value.element else {
			return XCTFail("No Element")
		}
		
		XCTAssertEqual(expected, result)
	}
}

